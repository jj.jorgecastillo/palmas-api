import { CreateAssignmentUsehersDto } from '../dto/create-assignments-usehers.dto';
import { UpdateAssignmentUsehersDto } from '../dto/update-assignment-usehers.dto';
import { AssignmentUsehers } from '../entities/assignment-usehers.entity';
import { Repository } from 'typeorm';
export declare class AssignmentsUsehersService {
    private readonly assignmentRepository;
    constructor(assignmentRepository: Repository<AssignmentUsehers>);
    create(createAssignmentDto: CreateAssignmentUsehersDto): Promise<CreateAssignmentUsehersDto & AssignmentUsehers>;
    findAll(): Promise<AssignmentUsehers[]>;
    findOne(id: number): string;
    update(id: number, updateAssignmentDto: UpdateAssignmentUsehersDto): string;
    remove(id: number): string;
}
