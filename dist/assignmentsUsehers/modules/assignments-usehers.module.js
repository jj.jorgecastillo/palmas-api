"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignmentsUsehersModule = void 0;
const common_1 = require("@nestjs/common");
const assignments_usehers_service_1 = require("../services/assignments-usehers.service");
const assignments_usehers_controller_1 = require("../controllers/assignments-usehers.controller");
const typeorm_1 = require("@nestjs/typeorm");
const assignment_usehers_entity_1 = require("../entities/assignment-usehers.entity");
const auth_module_1 = require("../../auth/modules/auth.module");
let AssignmentsUsehersModule = exports.AssignmentsUsehersModule = class AssignmentsUsehersModule {
};
exports.AssignmentsUsehersModule = AssignmentsUsehersModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([assignment_usehers_entity_1.AssignmentUsehers]), auth_module_1.AuthModule],
        controllers: [assignments_usehers_controller_1.AssignmentsUsehersController],
        providers: [assignments_usehers_service_1.AssignmentsUsehersService],
    })
], AssignmentsUsehersModule);
//# sourceMappingURL=assignments-usehers.module.js.map