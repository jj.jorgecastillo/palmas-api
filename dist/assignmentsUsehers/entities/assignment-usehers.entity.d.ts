import { Categories } from 'src/common/enums/category.enum';
export declare class AssignmentUsehers {
    id: number;
    category: Categories;
    meetingDate: Date;
    VIDEO?: number;
    AUDIO?: number;
    ZOOM_USHER?: number;
}
