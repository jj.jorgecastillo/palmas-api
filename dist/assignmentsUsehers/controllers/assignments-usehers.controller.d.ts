import { AssignmentsUsehersService } from '../services/assignments-usehers.service';
import { CreateAssignmentUsehersDto } from '../dto/create-assignments-usehers.dto';
import { UpdateAssignmentUsehersDto } from '../dto/update-assignment-usehers.dto';
export declare class AssignmentsUsehersController {
    private readonly assignmentsService;
    constructor(assignmentsService: AssignmentsUsehersService);
    create(createAssignmentDto: CreateAssignmentUsehersDto): Promise<CreateAssignmentUsehersDto & import("../entities/assignment-usehers.entity").AssignmentUsehers>;
    findAll(): Promise<import("../entities/assignment-usehers.entity").AssignmentUsehers[]>;
    findOne(id: string): string;
    update(id: string, updateAssignmentDto: UpdateAssignmentUsehersDto): string;
    remove(id: string): string;
}
