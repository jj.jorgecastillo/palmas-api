"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignmentsUsehersController = void 0;
const common_1 = require("@nestjs/common");
const assignments_usehers_service_1 = require("../services/assignments-usehers.service");
const create_assignments_usehers_dto_1 = require("../dto/create-assignments-usehers.dto");
const update_assignment_usehers_dto_1 = require("../dto/update-assignment-usehers.dto");
const auth_decorator_1 = require("../../common/decorators/auth.decorator");
const rol_enum_1 = require("../../common/enums/rol.enum");
const { ADMIN } = rol_enum_1.Role;
let AssignmentsUsehersController = exports.AssignmentsUsehersController = class AssignmentsUsehersController {
    constructor(assignmentsService) {
        this.assignmentsService = assignmentsService;
    }
    create(createAssignmentDto) {
        return this.assignmentsService.create(createAssignmentDto);
    }
    async findAll() {
        const data = await this.assignmentsService.findAll();
        return data;
    }
    findOne(id) {
        return this.assignmentsService.findOne(+id);
    }
    update(id, updateAssignmentDto) {
        return this.assignmentsService.update(+id, updateAssignmentDto);
    }
    remove(id) {
        return this.assignmentsService.remove(+id);
    }
};
__decorate([
    (0, auth_decorator_1.Auth)(ADMIN),
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_assignments_usehers_dto_1.CreateAssignmentUsehersDto]),
    __metadata("design:returntype", void 0)
], AssignmentsUsehersController.prototype, "create", null);
__decorate([
    (0, common_1.Get)('/usehers'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AssignmentsUsehersController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)('/usehers/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AssignmentsUsehersController.prototype, "findOne", null);
__decorate([
    (0, auth_decorator_1.Auth)(ADMIN),
    (0, common_1.Patch)('/usehers/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_assignment_usehers_dto_1.UpdateAssignmentUsehersDto]),
    __metadata("design:returntype", void 0)
], AssignmentsUsehersController.prototype, "update", null);
__decorate([
    (0, auth_decorator_1.Auth)(ADMIN),
    (0, common_1.Delete)('/usehers/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AssignmentsUsehersController.prototype, "remove", null);
exports.AssignmentsUsehersController = AssignmentsUsehersController = __decorate([
    (0, common_1.Controller)('assignments'),
    __metadata("design:paramtypes", [assignments_usehers_service_1.AssignmentsUsehersService])
], AssignmentsUsehersController);
//# sourceMappingURL=assignments-usehers.controller.js.map