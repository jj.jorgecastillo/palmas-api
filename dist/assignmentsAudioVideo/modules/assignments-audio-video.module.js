"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignmentsAudioVideoModule = void 0;
const common_1 = require("@nestjs/common");
const assignments_audio_video_service_1 = require("../services/assignments-audio-video.service");
const assignments_audio_video_controller_1 = require("../controllers/assignments-audio-video.controller");
const typeorm_1 = require("@nestjs/typeorm");
const assignments_audio_video_entity_1 = require("../entities/assignments-audio-video.entity");
const auth_module_1 = require("../../auth/modules/auth.module");
let AssignmentsAudioVideoModule = exports.AssignmentsAudioVideoModule = class AssignmentsAudioVideoModule {
};
exports.AssignmentsAudioVideoModule = AssignmentsAudioVideoModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([assignments_audio_video_entity_1.AssignmentAudioVideo]), auth_module_1.AuthModule],
        controllers: [assignments_audio_video_controller_1.AssignmentsControllerAudioVideo],
        providers: [assignments_audio_video_service_1.AssignmentAudioVideoService],
    })
], AssignmentsAudioVideoModule);
//# sourceMappingURL=assignments-audio-video.module.js.map