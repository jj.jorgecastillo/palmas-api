import { CreateAssignmentAudioVideoDto } from '../dto/create-assignments-audio-video.dto';
import { UpdateAssignmentAudioVideoDto } from '../dto/update-assignments-audio-video.dto';
import { AssignmentAudioVideo } from '../entities/assignments-audio-video.entity';
import { Repository } from 'typeorm';
export declare class AssignmentAudioVideoService {
    private readonly assignmentRepository;
    constructor(assignmentRepository: Repository<AssignmentAudioVideo>);
    create(createAssignmentDto: CreateAssignmentAudioVideoDto): Promise<CreateAssignmentAudioVideoDto & AssignmentAudioVideo>;
    findAll(): Promise<AssignmentAudioVideo[]>;
    findOne(id: number): string;
    update(id: number, updateAssignmentDto: UpdateAssignmentAudioVideoDto): string;
    remove(id: number): string;
}
