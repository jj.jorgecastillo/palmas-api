import { Categories } from 'src/common/enums/category.enum';
export declare class CreateAssignmentAudioVideoDto {
    meetingDate: Date;
    category: Categories;
    VIDEO?: number;
    AUDIO?: number;
    ZOOM_USHER?: number;
}
