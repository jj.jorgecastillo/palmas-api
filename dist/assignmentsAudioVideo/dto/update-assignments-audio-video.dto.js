"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAssignmentAudioVideoDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const create_assignments_audio_video_dto_1 = require("./create-assignments-audio-video.dto");
class UpdateAssignmentAudioVideoDto extends (0, mapped_types_1.PartialType)(create_assignments_audio_video_dto_1.CreateAssignmentAudioVideoDto) {
}
exports.UpdateAssignmentAudioVideoDto = UpdateAssignmentAudioVideoDto;
//# sourceMappingURL=update-assignments-audio-video.dto.js.map