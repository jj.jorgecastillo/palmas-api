import { CreateAssignmentAudioVideoDto } from './create-assignments-audio-video.dto';
declare const UpdateAssignmentAudioVideoDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateAssignmentAudioVideoDto>>;
export declare class UpdateAssignmentAudioVideoDto extends UpdateAssignmentAudioVideoDto_base {
}
export {};
