import { AssignmentAudioVideoService } from '../services/assignments-audio-video.service';
import { CreateAssignmentAudioVideoDto } from '../dto/create-assignments-audio-video.dto';
import { UpdateAssignmentAudioVideoDto } from '../dto/update-assignments-audio-video.dto';
export declare class AssignmentsControllerAudioVideo {
    private readonly assignmentsService;
    constructor(assignmentsService: AssignmentAudioVideoService);
    create(createAssignmentDto: CreateAssignmentAudioVideoDto): Promise<CreateAssignmentAudioVideoDto & import("../entities/assignments-audio-video.entity").AssignmentAudioVideo>;
    findAll(): Promise<import("../entities/assignments-audio-video.entity").AssignmentAudioVideo[]>;
    findOne(id: string): string;
    update(id: string, updateAssignmentDto: UpdateAssignmentAudioVideoDto): string;
    remove(id: string): string;
}
