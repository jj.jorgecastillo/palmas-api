import { AuthService } from '../services/auth.service';
import { LoginDto } from '../dto/login.dto';
import { RegisterDto } from '../dto/register.dto';
import { Role } from '../../common/enums/rol.enum';
import { UserActiveInterface } from 'src/common/interface/user-active.interface';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    register(registerDto: RegisterDto): Promise<{
        name: string;
        secondName: string;
        surname: string;
        lastName: string;
        email: string;
        group: import("../../common/enums/groups.enum").Groups;
        christened: boolean;
        gender: import("../../common/enums/gender.enum").Gender;
        phoneNumber: string;
    }>;
    login(loginDto: LoginDto): Promise<{
        token: string;
        email: string;
        role: Role;
    }>;
    profile(user: UserActiveInterface): Promise<import("../../users/entities/user.entity").User>;
}
