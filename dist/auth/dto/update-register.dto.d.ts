import { RegisterDto } from './register.dto';
declare const UpdateRegisterDto_base: import("@nestjs/mapped-types").MappedType<Partial<RegisterDto>>;
export declare class UpdateRegisterDto extends UpdateRegisterDto_base {
}
export {};
