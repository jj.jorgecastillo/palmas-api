import { UsersService } from 'src/users/services/users.service';
import { RegisterDto } from '../dto/register.dto';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from '../dto/login.dto';
export declare class AuthService {
    private readonly usersService;
    private readonly jwtService;
    constructor(usersService: UsersService, jwtService: JwtService);
    register({ name, secondName, surname, lastName, group, email, password, christened, gender, phoneNumber, }: RegisterDto): Promise<{
        name: string;
        secondName: string;
        surname: string;
        lastName: string;
        email: string;
        group: import("../../common/enums/groups.enum").Groups;
        christened: boolean;
        gender: import("../../common/enums/gender.enum").Gender;
        phoneNumber: string;
    }>;
    login({ email, password }: LoginDto): Promise<{
        token: string;
        email: string;
        role: import("../../common/enums/rol.enum").Role;
    }>;
    profile({ email }: {
        email: string;
    }): Promise<import("../../users/entities/user.entity").User>;
}
