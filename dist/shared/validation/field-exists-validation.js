"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FieldExists = exports.FieldExistsRule = void 0;
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const antd_validation_constants_1 = require("./antd-validation.constants");
let FieldExistsRule = exports.FieldExistsRule = class FieldExistsRule {
    async validate(fieldvalue, parameters) {
        const { property: { fieldName: defaultFieldName }, constraints: [{ repositoryName, parentName = [], fieldName: fieldNameAlias },], } = parameters;
        const fieldName = fieldNameAlias || defaultFieldName;
        const query = this.buildQuery({ fieldName, parentName, fieldvalue });
        return !(await { repositoryName, query });
    }
    defaultMessage() {
        return antd_validation_constants_1.ALREADY_EXISTS;
    }
    buildQuery({ parentName, fieldName, fieldvalue }) {
        const query = {};
        const property = this.buildName({
            parentName,
            fieldName,
        });
        query[property] = fieldvalue;
        return query;
    }
    buildName({ parentName = [], fieldName }) {
        return [...parentName, fieldName].join('.');
    }
};
exports.FieldExistsRule = FieldExistsRule = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'FieldExistsRule', async: true }),
    (0, common_1.Injectable)()
], FieldExistsRule);
function FieldExists(property, validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            name: 'FieldExists',
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [property],
            validator: FieldExistsRule,
        });
    };
}
exports.FieldExists = FieldExists;
//# sourceMappingURL=field-exists-validation.js.map