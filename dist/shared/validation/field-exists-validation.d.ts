import { ValidationOptions, ValidatorConstraintInterface } from 'class-validator';
export declare class FieldExistsRule implements ValidatorConstraintInterface {
    validate(fieldvalue: string, parameters: any): Promise<boolean>;
    defaultMessage(): string;
    private buildQuery;
    private buildName;
}
export declare function FieldExists(property: FieldExistsProperties, validationOptions?: ValidationOptions): (object: any, propertyName: string) => void;
interface FieldExistsProperties {
    repositoryName: string;
    parentName?: any[];
    fieldName?: string;
}
export {};
