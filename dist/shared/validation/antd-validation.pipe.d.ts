import { ValidationPipe, BadRequestException, ValidationPipeOptions } from '@nestjs/common';
import { ValidationError } from 'class-validator';
export declare class AntdValidationPipe extends ValidationPipe {
    constructor(options?: ValidationPipeOptions);
    formatErrorsToAntdForm(errors: ValidationError[]): BadRequestException;
    private formatConstraints;
}
