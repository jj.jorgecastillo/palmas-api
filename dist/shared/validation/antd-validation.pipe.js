"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntdValidationPipe = void 0;
const common_1 = require("@nestjs/common");
class AntdValidationPipe extends common_1.ValidationPipe {
    constructor(options = {}) {
        super({
            stopAtFirstError: true,
            validationError: { target: false },
            exceptionFactory: errors => this.formatErrorsToAntdForm(errors),
            whitelist: true,
            ...options,
        });
    }
    formatErrorsToAntdForm(errors) {
        return new common_1.BadRequestException(errors.map(error => this.formatConstraints(error)).flat());
    }
    formatConstraints(error, prefix) {
        if (error.children.length) {
            return error.children.map(child => this.formatConstraints(child, error.property));
        }
        return {
            name: prefix ? `${prefix}.${error.property}` : error.property,
            errors: Object.values(error.constraints),
        };
    }
}
exports.AntdValidationPipe = AntdValidationPipe;
//# sourceMappingURL=antd-validation.pipe.js.map