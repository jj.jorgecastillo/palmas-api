"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const config_1 = require("@nestjs/config");
const auth_module_1 = require("./auth/modules/auth.module");
const users_module_1 = require("./users/modules/users.module");
const assignments_module_1 = require("./assignments/modules/assignments.module");
const assignments_usehers_module_1 = require("./assignmentsUsehers/modules/assignments-usehers.module");
const assignments_audio_video_module_1 = require("./assignmentsAudioVideo/modules/assignments-audio-video.module");
const assignments_microphones_module_1 = require("./assignmentsMicrophones/modules/assignments-microphones.module");
let AppModule = exports.AppModule = class AppModule {
};
exports.AppModule = AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true,
            }),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                host: process.env.POSTGRES_HOST,
                port: parseInt(process.env.POSTGRES_PORT) || 5436,
                username: process.env.POSTGRES_USERNAME,
                password: process.env.POSTGRES_PASSWORD,
                database: process.env.POSTGRES_DATABASE,
                autoLoadEntities: true,
                synchronize: true,
                ssl: process.env.POSTGRES_SSL === 'true',
                extra: {
                    ssl: process.env.POSTGRES_SSL === 'true'
                        ? { rejectUnauthorized: false }
                        : null,
                },
            }),
            auth_module_1.AuthModule,
            users_module_1.UsersModule,
            assignments_module_1.AssignmentsModule,
            assignments_usehers_module_1.AssignmentsUsehersModule,
            assignments_audio_video_module_1.AssignmentsAudioVideoModule,
            assignments_microphones_module_1.AssignmentsMicrophonesModule,
        ],
        controllers: [],
        providers: [],
    })
], AppModule);
//# sourceMappingURL=app.module.js.map