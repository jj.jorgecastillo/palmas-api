import { Repository } from 'typeorm';
import { CreateAssignmentMicrophonesDto } from '../dto/create-assignments-microphones.dto';
import { UpdateAssignmentMicrophonesDto } from '../dto/update-assignments-microphones.dto';
import { AssignmentMicrophones } from '../entities/assignments-microphones.entity';
export declare class AssignmentsMicrophonesService {
    private readonly assignmentRepository;
    constructor(assignmentRepository: Repository<AssignmentMicrophones>);
    create(createAssignmentDto: CreateAssignmentMicrophonesDto): Promise<CreateAssignmentMicrophonesDto & AssignmentMicrophones>;
    findAll(): Promise<AssignmentMicrophones[]>;
    findOne(id: number): string;
    update(id: number, updateAssignmentDto: UpdateAssignmentMicrophonesDto): string;
    remove(id: number): string;
}
