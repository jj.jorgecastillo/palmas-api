"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignmentsMicrophonesModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const assignments_microphones_service_1 = require("../services/assignments-microphones.service");
const assignments_microphones_controller_1 = require("../controllers/assignments-microphones.controller");
const assignments_microphones_entity_1 = require("../entities/assignments-microphones.entity");
const auth_module_1 = require("../../auth/modules/auth.module");
let AssignmentsMicrophonesModule = exports.AssignmentsMicrophonesModule = class AssignmentsMicrophonesModule {
};
exports.AssignmentsMicrophonesModule = AssignmentsMicrophonesModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([assignments_microphones_entity_1.AssignmentMicrophones]), auth_module_1.AuthModule],
        controllers: [assignments_microphones_controller_1.AssignmentsController],
        providers: [assignments_microphones_service_1.AssignmentsMicrophonesService],
    })
], AssignmentsMicrophonesModule);
//# sourceMappingURL=assignments-microphones.module.js.map