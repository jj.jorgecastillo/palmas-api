"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAssignmentMicrophonesDto = void 0;
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const category_enum_1 = require("../../common/enums/category.enum");
class CreateAssignmentMicrophonesDto {
}
exports.CreateAssignmentMicrophonesDto = CreateAssignmentMicrophonesDto;
__decorate([
    (0, class_validator_1.IsDate)(),
    (0, class_transformer_1.Type)(() => Date),
    __metadata("design:type", Date)
], CreateAssignmentMicrophonesDto.prototype, "meetingDate", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(category_enum_1.Categories),
    __metadata("design:type", String)
], CreateAssignmentMicrophonesDto.prototype, "category", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentMicrophonesDto.prototype, "MICRO_1", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentMicrophonesDto.prototype, "MICRO_2", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentMicrophonesDto.prototype, "PLATFORM", void 0);
//# sourceMappingURL=create-assignments-microphones.dto.js.map