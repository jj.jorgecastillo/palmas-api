"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAssignmentMicrophonesDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const create_assignments_microphones_dto_1 = require("./create-assignments-microphones.dto");
class UpdateAssignmentMicrophonesDto extends (0, mapped_types_1.PartialType)(create_assignments_microphones_dto_1.CreateAssignmentMicrophonesDto) {
}
exports.UpdateAssignmentMicrophonesDto = UpdateAssignmentMicrophonesDto;
//# sourceMappingURL=update-assignments-microphones.dto.js.map