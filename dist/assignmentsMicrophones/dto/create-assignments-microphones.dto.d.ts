import { Categories } from 'src/common/enums/category.enum';
export declare class CreateAssignmentMicrophonesDto {
    meetingDate: Date;
    category: Categories;
    MICRO_1?: number;
    MICRO_2?: number;
    PLATFORM?: number;
}
