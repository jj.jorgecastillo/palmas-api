"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignmentMicrophones = void 0;
const category_enum_1 = require("../../common/enums/category.enum");
const typeorm_1 = require("typeorm");
let AssignmentMicrophones = exports.AssignmentMicrophones = class AssignmentMicrophones {
};
__decorate([
    (0, typeorm_1.Column)({ primary: true, generated: true }),
    __metadata("design:type", Number)
], AssignmentMicrophones.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'enum', default: category_enum_1.Categories.MICROPHONE, enum: category_enum_1.Categories }),
    __metadata("design:type", String)
], AssignmentMicrophones.prototype, "category", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Date)
], AssignmentMicrophones.prototype, "meetingDate", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: null }),
    __metadata("design:type", Number)
], AssignmentMicrophones.prototype, "MICRO_1", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: null }),
    __metadata("design:type", Number)
], AssignmentMicrophones.prototype, "MICRO_2", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: null }),
    __metadata("design:type", Number)
], AssignmentMicrophones.prototype, "PLATFORM", void 0);
exports.AssignmentMicrophones = AssignmentMicrophones = __decorate([
    (0, typeorm_1.Entity)()
], AssignmentMicrophones);
//# sourceMappingURL=assignments-microphones.entity.js.map