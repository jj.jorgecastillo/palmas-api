import { Categories } from 'src/common/enums/category.enum';
export declare class AssignmentMicrophones {
    id: number;
    category: Categories;
    meetingDate: Date;
    MICRO_1?: number;
    MICRO_2?: number;
    PLATFORM?: number;
}
