import { AssignmentsMicrophonesService } from '../services/assignments-microphones.service';
import { CreateAssignmentMicrophonesDto } from '../dto/create-assignments-microphones.dto';
import { UpdateAssignmentMicrophonesDto } from '../dto/update-assignments-microphones.dto';
export declare class AssignmentsController {
    private readonly assignmentsService;
    constructor(assignmentsService: AssignmentsMicrophonesService);
    create(createAssignmentDto: CreateAssignmentMicrophonesDto): Promise<CreateAssignmentMicrophonesDto & import("../entities/assignments-microphones.entity").AssignmentMicrophones>;
    findAll(): Promise<import("../entities/assignments-microphones.entity").AssignmentMicrophones[]>;
    findOne(id: string): string;
    update(id: string, updateAssignmentDto: UpdateAssignmentMicrophonesDto): string;
    remove(id: string): string;
}
