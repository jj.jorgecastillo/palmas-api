"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const validation_1 = require("./shared/validation");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.setGlobalPrefix('api/v1');
    app.useGlobalPipes(new validation_1.AntdValidationPipe({ transform: true })).enableCors();
    await app.listen(parseInt(process.env.PORT) || 4000);
}
bootstrap();
//# sourceMappingURL=main.js.map