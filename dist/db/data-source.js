"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dataSourceOptions = void 0;
const typeorm_1 = require("typeorm");
const dotenv = require("dotenv");
dotenv.config();
exports.dataSourceOptions = {
    type: 'mysql',
    host: process.env.DB_MYSQL_HOST,
    database: process.env.DB_MYSQL_NAME,
    password: process.env.DB_MYSQL_PASSWORD,
    port: +process.env.DB_MYSQL_PORT,
    username: process.env.DB_MYSQL_USERNAME,
    entities: ['dist/**/*.entity.js'],
    migrations: ['dist/db/migrations/*.js'],
};
const dataSource = new typeorm_1.DataSource(exports.dataSourceOptions);
exports.default = dataSource;
//# sourceMappingURL=data-source.js.map