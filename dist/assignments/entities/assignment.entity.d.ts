export declare class Assignment {
    id: number;
    category: string;
    meetingDate: Date;
    USHER_1?: number;
    USHER_2?: number;
    EXTERNAL_USHER?: number;
    MICRO_1?: number;
    MICRO_2?: number;
    PLATFORM?: number;
    VIDEO?: number;
    AUDIO?: number;
    ZOOM_USHER?: number;
}
