"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAssignmentDto = exports.Categories = void 0;
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
var Categories;
(function (Categories) {
    Categories["Useher"] = "Usehers";
    Categories["Microphone"] = "Microphones";
    Categories["AudioVideo"] = "AudioVideo";
})(Categories || (exports.Categories = Categories = {}));
class CreateAssignmentDto {
}
exports.CreateAssignmentDto = CreateAssignmentDto;
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "USHER_1", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "USHER_2", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "EXTERNAL_USHER", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "MICRO_1", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "MICRO_2", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "PLATFORM", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "VIDEO", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "AUDIO", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Number)
], CreateAssignmentDto.prototype, "ZOOM_USHER", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(Categories),
    __metadata("design:type", String)
], CreateAssignmentDto.prototype, "category", void 0);
__decorate([
    (0, class_validator_1.IsDate)(),
    (0, class_transformer_1.Type)(() => Date),
    __metadata("design:type", Date)
], CreateAssignmentDto.prototype, "meetingDate", void 0);
//# sourceMappingURL=create-assignment.dto.js.map