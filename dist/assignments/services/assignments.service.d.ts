import { CreateAssignmentDto } from '../dto/create-assignment.dto';
import { UpdateAssignmentDto } from '../dto/update-assignment.dto';
import { Assignment } from '../entities/assignment.entity';
import { Repository } from 'typeorm';
export declare class AssignmentsService {
    private readonly assignmentRepository;
    constructor(assignmentRepository: Repository<Assignment>);
    create(createAssignmentDto: CreateAssignmentDto): Promise<CreateAssignmentDto & Assignment>;
    findAll(): Promise<Assignment[]>;
    findOne(id: number): string;
    update(id: number, updateAssignmentDto: UpdateAssignmentDto): string;
    remove(id: number): string;
}
