import { AssignmentsService } from '../services/assignments.service';
import { CreateAssignmentDto } from '../dto/create-assignment.dto';
import { UpdateAssignmentDto } from '../dto/update-assignment.dto';
export declare class AssignmentsController {
    private readonly assignmentsService;
    constructor(assignmentsService: AssignmentsService);
    create(createAssignmentDto: CreateAssignmentDto): Promise<CreateAssignmentDto & import("../entities/assignment.entity").Assignment>;
    findAll(): Promise<unknown[]>;
    findAllPlain(): Promise<unknown[]>;
    findOne(id: string): string;
    update(id: string, updateAssignmentDto: UpdateAssignmentDto): string;
    remove(id: string): string;
}
