"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssignmentsController = void 0;
const common_1 = require("@nestjs/common");
const assignments_service_1 = require("../services/assignments.service");
const create_assignment_dto_1 = require("../dto/create-assignment.dto");
const update_assignment_dto_1 = require("../dto/update-assignment.dto");
const auth_decorator_1 = require("../../common/decorators/auth.decorator");
const rol_enum_1 = require("../../common/enums/rol.enum");
const { ADMIN } = rol_enum_1.Role;
let AssignmentsController = exports.AssignmentsController = class AssignmentsController {
    constructor(assignmentsService) {
        this.assignmentsService = assignmentsService;
    }
    create(createAssignmentDto) {
        return this.assignmentsService.create(createAssignmentDto);
    }
    async findAll() {
        const data = await this.assignmentsService.findAll();
        const groupedData = {};
        data.forEach((assignment) => {
            const dateKey = assignment.meetingDate.toISOString().slice(0, 10);
            if (!groupedData[dateKey]) {
                groupedData[dateKey] = {
                    key: assignment.id,
                    meetingDate: dateKey,
                    assignations: [],
                };
            }
            const users = Object.entries(assignment)
                .filter(([key, value]) => key !== 'id' && key !== 'category' && value !== null)
                .map(([key, value]) => ({
                broth: value,
                assignation: key,
            }));
            if (users.length > 0) {
                users.shift();
            }
            groupedData[dateKey].assignations.push({
                category: assignment.category,
                users,
            });
        });
        const result = Object.values(groupedData);
        result.sort((a, b) => {
            const dateA = new Date(a.meetingDate);
            const dateB = new Date(b.meetingDate);
            return dateA.getTime() - dateB.getTime();
        });
        console.dir(result, { depth: null });
        return result;
    }
    async findAllPlain() {
        const assignments = await this.assignmentsService.findAll();
        const consolidatedAssignments = {};
        assignments.forEach((assignment) => {
            const meetingDate = assignment.meetingDate.toISOString().slice(0, 10);
            if (!consolidatedAssignments[meetingDate]) {
                consolidatedAssignments[meetingDate] = {
                    meetingDate,
                };
            }
            for (const key in assignment) {
                if (key !== 'id' && key !== 'category') {
                    if (assignment[key] !== null) {
                        consolidatedAssignments[meetingDate][key] = assignment[key];
                    }
                }
            }
        });
        const result = Object.values(consolidatedAssignments);
        result.sort((a, b) => a.meetingDate - b.meetingDate);
        return result;
    }
    findOne(id) {
        return this.assignmentsService.findOne(+id);
    }
    update(id, updateAssignmentDto) {
        return this.assignmentsService.update(+id, updateAssignmentDto);
    }
    remove(id) {
        return this.assignmentsService.remove(+id);
    }
};
__decorate([
    (0, auth_decorator_1.Auth)(ADMIN),
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_assignment_dto_1.CreateAssignmentDto]),
    __metadata("design:returntype", void 0)
], AssignmentsController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AssignmentsController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)('/plain'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AssignmentsController.prototype, "findAllPlain", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AssignmentsController.prototype, "findOne", null);
__decorate([
    (0, auth_decorator_1.Auth)(ADMIN),
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_assignment_dto_1.UpdateAssignmentDto]),
    __metadata("design:returntype", void 0)
], AssignmentsController.prototype, "update", null);
__decorate([
    (0, auth_decorator_1.Auth)(ADMIN),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AssignmentsController.prototype, "remove", null);
exports.AssignmentsController = AssignmentsController = __decorate([
    (0, common_1.Controller)('assignments'),
    __metadata("design:paramtypes", [assignments_service_1.AssignmentsService])
], AssignmentsController);
//# sourceMappingURL=assignments.controller.js.map