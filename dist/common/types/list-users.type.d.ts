export type TListUsers = {
    surname: string;
    name: string;
    id: number;
    group: number;
};
