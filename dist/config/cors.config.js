"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
exports.default = (0, config_1.registerAs)('cors', () => ({
    origin: process.env.CORS_ORIGINS || '*',
    methods: 'GET,PUT,POST,DELETE,PATCH',
    allowedHeaders: 'Content-Type,Authorization',
}));
//# sourceMappingURL=cors.config.js.map