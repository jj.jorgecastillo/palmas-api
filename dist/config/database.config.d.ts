import { DataSourceOptions } from 'typeorm';
type DatabaseConfig = {
    mySql: DataSourceOptions;
};
declare const _default: (() => DatabaseConfig) & import("@nestjs/config").ConfigFactoryKeyHost<DatabaseConfig>;
export default _default;
