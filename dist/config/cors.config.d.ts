import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
declare const _default: (() => CorsOptions) & import("@nestjs/config").ConfigFactoryKeyHost<CorsOptions>;
export default _default;
