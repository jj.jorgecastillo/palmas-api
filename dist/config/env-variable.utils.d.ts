export declare function ensureEnvVar(variableName: string): string;
export declare function validateNumberEnvVar(variableName: string, defaultValue?: number): number;
