export type ApiConfig = {
    env: string;
    port: number;
    webBaseUrl: string;
};
declare const _default: (() => ApiConfig) & import("@nestjs/config").ConfigFactoryKeyHost<ApiConfig>;
export default _default;
