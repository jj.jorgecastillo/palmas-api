"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const env_variable_utils_1 = require("./env-variable.utils");
exports.default = (0, config_1.registerAs)('api', () => {
    return {
        env: process.env.NODE_ENV || 'development',
        port: (0, env_variable_utils_1.validateNumberEnvVar)('PORT', 3001),
        webBaseUrl: (0, env_variable_utils_1.ensureEnvVar)('WEB_BASE_URL'),
    };
});
//# sourceMappingURL=api.config.js.map