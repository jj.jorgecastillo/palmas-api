"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateNumberEnvVar = exports.ensureEnvVar = void 0;
function ensureEnvVar(variableName) {
    const value = process.env[variableName];
    if (!value) {
        throw new Error(`Please set "${variableName}" env variable`);
    }
    return value;
}
exports.ensureEnvVar = ensureEnvVar;
function validateNumberEnvVar(variableName, defaultValue) {
    const value = parseInt(process.env[variableName], 10) || defaultValue;
    if (Number.isNaN(value)) {
        throw new Error(`Please set a valid "${variableName}" env variable`);
    }
    return value;
}
exports.validateNumberEnvVar = validateNumberEnvVar;
//# sourceMappingURL=env-variable.utils.js.map