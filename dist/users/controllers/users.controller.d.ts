import { UsersService } from '../services/users.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { Gender } from 'src/common/enums/gender.enum';
interface GroupedData {
    [group: string]: {
        id: number;
        fullName: string;
        gender: Gender;
        phoneNumber: string;
        email: string;
    }[];
}
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUserDto: CreateUserDto): Promise<CreateUserDto & import("../entities/user.entity").User>;
    listUser(): Promise<GroupedData>;
    findAll(): Promise<import("../entities/user.entity").User[]>;
    findOne(email: string): Promise<import("../entities/user.entity").User>;
    update(id: string, updateUserDto: UpdateUserDto): string;
    remove(id: string): string;
}
export {};
