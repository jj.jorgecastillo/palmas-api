import { Gender } from 'src/common/enums/gender.enum';
import { Groups } from 'src/common/enums/groups.enum';
import { Role } from 'src/common/enums/rol.enum';
export declare class CreateUserDto {
    name: string;
    secondName?: string;
    surname: string;
    lastName?: string;
    email: string;
    group?: Groups;
    password: string;
    role?: Role;
    christened?: boolean;
    gender?: Gender;
    phoneNumber?: string;
}
