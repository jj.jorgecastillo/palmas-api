import { Gender } from 'src/common/enums/gender.enum';
import { Role } from 'src/common/enums/rol.enum';
import { Groups } from 'src/common/enums/groups.enum';
export declare class User {
    id: number;
    name: string;
    secondName?: string;
    surname: string;
    lastName?: string;
    christened?: boolean;
    phoneNumber?: string;
    email: string;
    password: string;
    role: Role;
    gender?: Gender;
    group?: Groups;
    deletedAt: Date;
}
